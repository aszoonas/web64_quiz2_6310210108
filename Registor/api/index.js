
const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'runner_admin',
    password : 'runner_admin',
    database : 'RunningSystem'
})

connection.connect()

const express = require('express')
const app = express()
const port = 4000

app.get("/list_event", (req, res) => {
    query = "SELECT * from Runner";
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({ 
                        "status" : "400" ,
                        "message" : "Error querying from running db"
                    })
        }else {
            res.json(rows)
        }
    })

})

app.listen(port, () => {
    console.log(`Now starting Running System Backend ${port} `)
})


/*
query = "SELECT * from Runner";
connection.query( query, (err, rows) => {
    if(err) {
        console.log(err)
    }else {
        console.log(rows)
    }
})

connection.end();
*/
